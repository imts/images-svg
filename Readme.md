# Travailler avec des images SVG dans vos pages Web

> Auteur : Gilles Gonon  
> Date : juin 2020

Ce dépôt contient des exemples et ressources pour inclure et animer des images SVG
dans vos sites Web. Ces exemples servent de bases au cours dispensés sur le thème à l'Institut Marie-Thérèse Solacroup.

Après avoir préparer ce cours, je suis tombé sur les Slides de Sara Soueidan qui présentent tout cela de manière très claire (en anglais) et fournit aussi de nombreux exemples, qui ont inspiré d'autres exemples de ce cours.

Un support de cours et des vidéos Youtube associés à ces exemples sont disponibles :

- [Cours sur les images SVG](https://imts.gitlab.io/cours-imts/02.web/html/images-svg/)
- Vidéos Youtube : Une [playlist spécifique SVG](https://www.youtube.com/watch?v=_n1W0vpmRPM&list=PLkSM07Hbh21lbJIacDtwm3c1qs-D3j9C2)

> Licence copyleft  
> Please code with poetry
